<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
	public $fillable = ['name', 'order', 'type', 'level', 'cost',
		'factor', 'parent_id', 'formula', 'varname', 'price', 'partnum'];

	public function childs() {
		return $this->hasMany('App\Category', 'parent_id', 'id');
	}
	public function parent() {
		return $this->belongsTo('App\Category', 'parent_id', 'id');
	}
}
