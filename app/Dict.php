<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dict extends Model
{
    public $fillable = ['key','value','comment'];

    public static function getRecord($key)
    {
    	return static::where('key', $key)->get();
    }
}
