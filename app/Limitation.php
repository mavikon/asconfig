<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Limitation extends Model {
	static public function subjExist($subj_id) {

		$lim = Limitation::where('subj_id', $subj_id)->get();

		if ($lim->isEmpty()) {
			return false;
		}

		return $lim;
	}

}
