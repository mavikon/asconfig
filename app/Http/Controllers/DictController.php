<?php

namespace App\Http\Controllers;
use App\Dict;
use Illuminate\Http\Request;

class DictController extends Controller {
	public function manageDict(Request $request) {
		$dicts = Dict::get();

		return view('manageDictview', compact('dicts'));
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Category  $category
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Dict $dict) {

		$input = $request->all();
		$dict = Dict::where('id', $input['id'])->first();

		$input['key'] = empty($input['key']) ? $dict['key'] : $input['key'];
		$input['value'] = empty($input['value']) ? $dict['value'] : $input['value'];
		$input['comment'] = empty($input['comment']) ? $dict['comment'] : $input['comment'];

		$updateNow = $dict->update($input);
		return back();
	}

	public function addDict(Request $request) {
		$this->validate($request, [
			'key' => 'required',
			'value' => 'required',
		]);
		$input = $request->all();
		$input['comment'] = empty($input['comment']) ? '-' : $input['comment'];

		Dict::create($input);
		return back()->with('success-section', 'Новый раздел добавлен.');
	}

	public function deleteRecord(Request $request) {
		$input = $request->all();

		if (Dict::where('id', $input['id'])->delete()) {
			$response = array('status' => 'success', 'msg' => 'Запись удалена.', 'id' => $input['id']);
			return \Response::json($response);
		}

		return 'no';

	}
}
