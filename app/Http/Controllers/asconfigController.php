<?php

namespace App\Http\Controllers;

/**
 * Для доступа к моделям Dict и Category
 */
use App\Category;
use App\Dict;
use App\Limitation;

class asconfigController extends Controller {

	public function main() {
		return view('index');
	}

	public function showCalcPage($par) {

		// в параметре наименование раздела. Получаем по нему первую запись
		$section = Category::where('name', '=', $par)
			->where('level', '=', 1) // берем только первый уровень, только раздел
			->first();
		if (!$section) {
			return view('404');
		}

		// так же нам нужны все категории - уровень 2
		// и все варианты для каждой категории - уровень 3
		$categories = Category::with('childs')->where('parent_id', $section->id)->get()->sortBy('order');

		foreach ($categories as $category) {
			foreach ($category->childs as $option) {
				// search $option->id in App\Limitation
				// if found, do something
				// if (Limitation::subjExist($option->id)) {
				$option->limits_by = Limitation::where('subj_id', $option->id)->get(['limit_id']);
				$option->limits = Limitation::where('limit_id', $option->id)->get(['subj_id']);
				// }
			}
		}
		// return $categories;

		// creating view here
		return view('CalcPage', compact('section', 'categories'));

	}
	// public function main(Request $request) {

	// 	dd($request);
	// 	// $dicts = Dict::get();
	// 	$categories = Category::where('parent_id', '=', null)->get();

	// 	// return $categories;
	// 	// return $dicts;
	// 	return view('main', compact('categories'));
	// }
}
