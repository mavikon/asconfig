<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller {

	public function manageCategory(Request $request) {
		$categories = Category::where('parent_id', '=', null)->get();
		/**
		 * $allCategories
		 */
		$allCategories = Category::with('childs')->where('type', 'SECTION')->get();
		$mainSections = Category::where('type', '=', 'SECTION')->pluck('name', 'id')->all();
		$mainCategories = Category::where('type', '=', 'CATEGORY')->pluck('name', 'id')->all();

		return view('categoryTreeview', compact('categories', 'allCategories', 'mainCategories', 'mainSections'));
	}

	public function addOption(Request $request) {
		$this->validate($request, [
			'variant_name' => 'required',
			'sel-cats' => 'required',
			'varname2' => 'required',
			'cost2' => 'required',
		]);
		$input = $request->all();
		$input['name'] = $input['variant_name'];
		$input['parent_id'] = $input['sel-cats'];
		$input['type'] = 'VARIANT';
		$input['level'] = '3';
		$input['varname'] = empty($input['varname2']) ? null : $input['varname2'];
		$input['cost'] = empty($input['cost2']) ? 0.0 : $input['cost2'];
		$input['factor'] = empty($input['factor2']) ? 1.0 : $input['factor2'];
		$input['price'] = empty($input['price']) ? 0.0 : $input['price'];
		$input['partnum'] = empty($input['partnum']) ? '' : $input['partnum'];
//dd($input);
		try {
			Category::create($input);
		} catch (\Exception $e) {
			return back()->withErrors($e->getMessage());
		}
		return back()->with('success-variant', 'Новый вариант добавлен.');
	}

	/**
	 * Добавление раздела.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function addSection(Request $request) {
		$this->validate($request, [
			'section_name' => 'required',
		]);
		$input = $request->all();
		$input['name'] = $input['section_name'];
		$input['type'] = 'SECTION';
		$input['level'] = '1';
		$input['formula'] = null;
		$input['varname'] = null;
		$input['cost'] = 0.0;
		$input['factor'] = 1.0;
		$input['parent_id'] = null;

		try {
			Category::create($input);
		} catch (\Exception $e) {
			return back()->withErrors($e->getMessage());
		}
		return back()->with('success-section', 'Новый раздел добавлен.');
	}

	/**
	 * Добавление категории в какой-либо раздел.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function addCategory(Request $request) {
		$this->validate($request, [
			'cat_name' => 'required',
			'parent_id' => 'required',
		]);
		$input = $request->all();
		$input['name'] = $input['cat_name'];
		$input['order'] = empty($input['order']) ? 0 : $input['order'];
		$input['parent_id'] = empty($input['parent_id']) ? 0 : $input['parent_id'];
		$input['type'] = 'CATEGORY';
		$input['level'] = '2';
		$input['formula'] = empty($input['formula']) ? null : $input['formula'];
		$input['varname'] = empty($input['varname']) ? null : $input['varname'];
		$input['cost'] = empty($input['cost']) ? 0.0 : $input['cost'];
		$input['factor'] = empty($input['factor']) ? 1.0 : $input['factor'];
		try {
			Category::create($input);
		} catch (\Exception $e) {
			return back()->withErrors($e->getMessage());
		}
		return back()->with('success-category', 'Новая категория добавлена.');
	}

	public function getByParentId($par) {
		return Category::where('parent_id', $par)->get()->all();
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$categories = Category::all();

		return view('categories.index', compact('categories'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Category  $category
	 * @return \Illuminate\Http\Response
	 */
	public function show(Category $category) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Category  $category
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Category $category) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Category  $category
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Category $category) {

		$input = $request->all();
		$cat = Category::where('id', $input['id'])->first();
		// $input['name'] = $input['cat_name'];
		// $input['parent_id'] = empty($input['parent_id']) ? 0 : $input['parent_id'];
		// $input['type'] = 'CATEGORY';
		// $input['level'] = '2';
		// $input['formula'] = empty($input['formula']) ? null : $input['formula'];
		// $input['varname'] = empty($input['varname']) ? null : $input['varname'];
		// $input['cost'] = empty($input['cost']) ? 0.0 : $input['cost'];
		// $input['factor'] = empty($input['factor']) ? 1.0 : $input['factor'];

		//dd($input);
		$updateNow = $cat->update($input);
		return back();

	}

	public function delete(Request $request) {
		$input = $request->all();

		$cat = Category::withCount('childs')->where('id', '=', $input['id'])->first();

		if ($cat->childs_count == 0) {
			$deleteNow = $cat->destroy($input);
			return back()->with('success-delete', 'Запись удалена.');
		} else {
			return back()->with('error-delete', 'Существуют дочерние элементы. Невозможно удалить запись.');
		}

	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Category  $category
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Category $category) {
		//
	}
}
