<?php

namespace App\Http\Controllers;
use App\Category;
use App\Limitation;
use Illuminate\Http\Request;

class LimitationController extends Controller {

	public function manageLimits(Request $request, $par = '') {
		$limits = Limitation::get();
		$allCategories = Category::with('childs')->where('type', 'SECTION')->get();

		foreach ($limits as $limit) {
			$limit->subj_name_pp = Category::where('id', $limit->subj_id)->first()->parent->parent->name;
			$limit->subj_name_p = Category::where('id', $limit->subj_id)->first()->parent->name;
			$limit->subj_name = Category::where('id', $limit->subj_id)->first()->name;

			$limit->limit_name_pp = Category::where('id', $limit->limit_id)->first()->parent->parent->name;
			$limit->limit_name_p = Category::where('id', $limit->limit_id)->first()->parent->name;
			$limit->limit_name = Category::where('id', $limit->limit_id)->first()->name;
		}

		//return $limits;
		return view('manageLimitsview', compact('limits', 'allCategories', 'par'));
	}

	public function addLimit(Request $request) {
		$this->validate($request, [
			'sel-subjId' => 'required',
			'sel-limitId' => 'required',
		]);
		$input = $request->all();

		$limit = new Limitation;
		$limit['subj_id'] = $input['sel-subjId'];
		$limit['limit_id'] = $input['sel-limitId'];
		$limit->save();

		return back()->with('success-limits', 'Ограничение добавлено.');
	}

	public function delLimit(Request $request) {
		$input = $request->all();
		if (Limitation::where('id', $input['id'])->delete()) {
			$response = array('status' => 'success', 'msg' => 'Запись удалена.', 'id' => $input['id']);
			return \Response::json($response);
		}

		return 'no';
	}
}
