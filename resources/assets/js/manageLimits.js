// require('./bootstrap');

var apiURL = '/getByParentId/';
var apiURL_POST = '/add-limit';
let token = document.head.querySelector('meta[name="csrf-token"]');

if (!token) {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

new Vue({
    el: '#vuejs-app',
    data: {
    	value: [],
    	checkedCategory1: [],
    	checkedCategory2: [],
        response1: null,
        response2: null,
        categorySelectedId1: null,
        categorySelectedId2: null,
        section: '',
    },
    methods: {
        activeSection: function(sectionName, par) {
            // console.log(par);
            if(sectionName == par)
                return false;

            return true;
        },
    	btnLimitsSave: function() {
    		// 1. Check if left panel NOT the same as right panel
    		if (typeof this.value[0] == 'undefined' ||
    				typeof this.value[1] == 'undefined' ||
    				this.checkedCategory1.length == 0 ||
        			this.checkedCategory2.length == 0)
    		{
    			alert('Необходимо выбрать категории и отметить варианты');
    			return false;
    		}
    		if(+this.value[0].split(' ', 1) == +this.value[1].split(' ', 1)) {
    			alert('Нельзя выбирать одинаковые категории');
    			return false;
    		}
    		//
    		// 2. if one or more checkboxes are checked at the left panel and at the right panel
    		// THEN do work, else exception handling
    		for( var i = 0, len2 = this.checkedCategory2.length; i < len2; i++) {
    			for ( var j = 0, len1 = this.checkedCategory1.length; j < len1; j++) {
    				this.pushData(this.checkedCategory1[j], this.checkedCategory2[i]);
    			}
    		}
    	},
    	pushData: function(option1, option2) {
    		var sendPar = JSON.stringify({"sel-subjId":option1,"sel-limitId":option2});
    		var xhr = new XMLHttpRequest();
    		var self = this;
    		xhr.open('POST', apiURL_POST);
    		xhr.setRequestHeader("Content-type", "application/json");
            xhr.setRequestHeader('X-CSRF-TOKEN', token.content);
    		xhr.onreadystatechange = function() {//Call a function when the state changes.
				if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
					// Request finished. Do processing here.
					console.log('Request finished. Data send: '+sendPar);
                    location.reload();
				}
			}
			xhr.send(sendPar);
    	},
    	fetchData: function(par, index) {
    		var xhr = new XMLHttpRequest();
    		var self = this;
    		xhr.open('GET', apiURL + par);
    		xhr.onload = function() {
    			if(index == 0)
    				self.response1 = JSON.parse(xhr.responseText);
    			else if(index == 1)
    				self.response2 = JSON.parse(xhr.responseText);
    		};
    		xhr.send();
    	},
    	optChanged: function(event, index) {
    		this.value[index] = event.target.value;
    		this.fetchData(this.value[index].split(' ', 1), index);
    	},
    },
});
