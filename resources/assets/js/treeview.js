$.fn.extend({
    treed: function (o) {
        var openedClass = 'fa-minus-circle';
        var closedClass = 'fa-plus-circle';

        if (typeof o != 'undefined') {
            if (typeof o.openedClass != 'undefined') {
                openedClass = o.openedClass;
            }
            if (typeof o.closedClass != 'undefined') {
                closedClass = o.closedClass;
            }
        };
        /* initialize each of the top levels */
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this);
            branch.prepend("");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                } else {
                }
            })
            branch.children().children().toggle();
        });
        $("li:not(:has(ul))").each(function() {
            $(this).on('click', function(event) {
                event.preventDefault();
                /* Act on the event */
                /**
                *  То, происходит при нажатии на самый "нижний" в данный момент элемент, не имеющий потомков
                *  Предполагалось, что именно такой элемент должен попадать в калькуляцию
                */
//                alert('ниже некуда');
            });;

        });
        tree.find('.branch i.fa').each(function () {
            $(this).on('click', function () {
                $(this).closest('li').click();
            });
        });
        /* fire event from the dynamically added icon */
        tree.find('.branch .indicator').each(function () {
            $(this).on('click', function () {
                $(this).closest('li').click();
            });
        });
        /* fire event to open branch if the li contains an anchor instead of text */
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        /* fire event to open branch if the li contains a button instead of text */
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});
/* Initialization of treeviews */
$('#tree1').treed();

$(document).ready(function(){
    // $("p").click(function(){
    //     $(this).hide();
    // });

    $(".btn-edit").click(function(e) {
        e.preventDefault();
        /* Act on the event */
        // alert('Edit!');
        $('#editSectionModal').attr("data-recordToEdit", $(this).attr('id'));
        //data-recordType
        $("#editSectionModal input[name='id']").val($(this).attr('data-id'));
        $("#editSectionModal input[name='name']").val($(this).attr('data-recordName'));
        $("#editSectionModal input[name='order']").val($(this).attr('data-recordOrder'));
        $("#editSectionModal input[name='type']").val($(this).attr('data-recordType'));
        $("#editSectionModal input[name='level']").val($(this).attr('data-recordLevel'));
        $("#editSectionModal input[name='formula']").val($(this).attr('data-recordFormula'));
        $("#editSectionModal input[name='varname']").val($(this).attr('data-recordVarname'));
        $("#editSectionModal input[name='cost']").val($(this).attr('data-recordCost'));
        $("#editSectionModal input[name='factor']").val($(this).attr('data-recordFactor'));
        $("#editSectionModal input[name='parent_id']").val($(this).attr('data-recordParentId'));
        $("#editSectionModal input[name='price']").val($(this).attr('data-recordPrice'));
        $("#editSectionModal input[name='partnum']").val($(this).attr('data-recordPartnum'));
        $('#editSectionModal').modal('show');
    });
    $(".btn-page").click(function(e) {
        e.preventDefault();
        /* Act on the event */
        window.location.href = "/" + $(this).attr('data-recordName');
    });
    $(".btn-limits").click(function(event) {
        event.preventDefault();
        /* Act on the event */
        window.location.href="/limits/" + $(this).attr('data-recordName');
    });

    $(".btn-delete").click(function(e) {
        e.preventDefault();
        /* Act on the event */
        if (+$(this).attr('data-hasChilds') > 0) {
            alert('Существуют дочерние элементы, удаление записи запрещено.');
        } else {
            $(".modal-delete-title").html("Удаление записи ID# " + '<code>' + $(this).attr('data-id') + '</code>');
            $('#deleteRecordModal').attr("data-recordToDelete", $(this).attr('id'));
            $("#deleteRecordModal input[name='id']").val($(this).attr('data-id'));
            $("#deleteRecordModal input[name='name']").val($(this).attr('data-recordName'));
            $('#deleteRecordModal').modal("show");
        }
    });

});