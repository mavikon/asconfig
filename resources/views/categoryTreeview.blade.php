<!DOCTYPE html>
<html>
<head>
	<title>ASConfig admin page</title>
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> --}}
    {{-- <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> --}}
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="/css/treeview.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>

	@include('include.navbar')

	@include('include.modal-edit')
	@include('include.modal-delete')
	@include('include.modal-info')

	<div class="container">
		<div class="panel panel-primary main-panel">
			<div class="panel-heading">Панель администратора ver. 0.0.1a</div>
			<div class="panel-body">
				<div class="row bottom-line">
					<div class="col-md-12">
						<h4>Справочник (key->value)</h4>
						<a href="/dict" class="btn btn-success" role="button">Справочник</a>
						<p></p>
					</div>
				</div>
			</div>
	  		<div class="panel-body">
	  		<div class="row">
	  			<div class="col-md-12">
					@if($errors->any())
						<p>{{$errors->first()}}</p>
					@endif
	  			</div>
	  		</div>
	  			<div class="row">
	  				<div class="col-md-6">
	  					<h3>Раздел-&gt;Категория-&gt;Вариант</h3>
				        <ul id="tree1">
				            @foreach($categories as $category)
				                <li>
                                    @if(count($category->childs))
                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                    @endif
				                    {{ $category->name }}
				                    <code id="delete-{{ $category->id }}"
				                    	  data-id="{{ $category->id }}"
				                    	  data-recordName="{{ $category->name }}"
				                    	  data-recordParentId="{{ $category->parent_id }}"
				                    	  data-hasChilds="{{ count($category->childs) }}"
				                    	  type="button"
				                    	  class="btn-delete btn-xs btn-default">Delete</code>
				                    <code id="edit-{{ $category->id }}"
				                    	  data-id="{{ $category->id }}"
				                    	  data-recordName="{{ $category->name }}"
				                    	  data-recordOrder="{{ $category->order }}"
				                    	  data-recordType="{{ $category->type }}"
				                    	  data-recordLevel="{{ $category->level }}"
				                    	  data-recordFormula="{{ $category->formula }}"
				                    	  data-recordVarname="{{ $category->varname }}"
				                    	  data-recordCost="{{ $category->cost }}"
				                    	  data-recordFactor="{{ $category->factor }}"
				                    	  data-recordParentId="{{ $category->parent_id }}"
				                    	  data-recordPrice="{{ $category->price }}"
				                    	  data-recordPartnum="{{ $category->partnum }}"
				                    	  type="button"
				                    	  class="btn-edit btn-xs btn-default">Edit</code>
				                   	<code id="limits-{{ $category->id }}"
				                    	  data-id="{{ $category->id }}"
				                   		  data-recordName="{{ $category->name }}"
										  type="button"
				                    	  class="btn-limits btn-xs btn-default">Limits</code>
				                    <code id="page-{{ $category->id }}"
				                    	  data-id="{{ $category->id }}"
				                    	  data-recordName="{{ $category->name }}"
				                    	  type="button"
				                    	  class="btn-page btn-xs btn-default">Page</code>
				                    @if(count($category->childs))
				                        @include('manageChild',['childs' => $category->childs])
				                    @endif
				                </li>
				            @endforeach
				        </ul>
	  				</div>
	  				<div class="col-md-6">
				    	@include('include.add-section')
						@include('include.add-category')
						@include('include.add-option')
	  				</div>
	  			</div>
	  		</div>
        </div>
    </div>
    <script src="/js/treeview.js"></script>
    <script src="/js/main.js"></script>
</body>
</html>