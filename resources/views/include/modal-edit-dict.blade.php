	<!-- Modal -->
	<div id="editDictModal" class="modal fade" role="dialog" data-recordToEdit="">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Редактирование записи</h4>
	      </div>
	      <div class="modal-body">
	        {!! Form::open(['route'=>'edit.dict', 'method' => 'put']) !!}
	        <div class="row">
	        	<div class="col-md-12">
					{!! Form::label('key', 'Ключ', ['class' => 'awesome-formlabel']) !!}
		        	{!! Form::text('key', old('key'), array('class'  => 'awesome-forminput')) !!}
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="col-md-12">
				{!! Form::label('value', 'Значение', ['class' => 'awesome-formlabel']) !!}
	        	{!! Form::text('value', old('value'), array('class'  => 'awesome-forminput')) !!}
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="col-md-12">
				{!! Form::label('comment', 'Комментарий', ['class' => 'awesome-formlabel']) !!}
	        	{!! Form::text('comment', old('comment'), array('class'  => 'awesome-forminput')) !!}
	        	</div>
	        </div>
	        	{!! Form::hidden('id') !!}
	        	<button class="btn btn-success">Сохранить</button>
	        {!! Form::close() !!}
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>
