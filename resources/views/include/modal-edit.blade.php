	<!-- Modal -->
	<div id="editSectionModal" class="modal fade" role="dialog" data-recordToEdit="">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Редактирование записи</h4>
	      </div>
	      <div class="modal-body">
	        {!! Form::open(['route'=>'edit.record', 'method' => 'put']) !!}
	        <div class="row">
	        	<div class="col-md-12">
					{!! Form::label('name', 'Название', ['class' => 'awesome-formlabel']) !!}
		        	{!! Form::text('name', '', array('class'  => 'awesome-forminput')) !!}
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="col-md-12">
					{!! Form::label('order', '# n/n', ['class' => 'awesome-formlabel']) !!}
		        	{!! Form::text('order', '', array('class'  => 'awesome-forminput')) !!}
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-12">
				{!! Form::label('type', 'Тип', ['class' => 'awesome-formlabel']) !!}
	        	{!! Form::text('type', '', array('class'  => 'awesome-forminput')) !!}
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="col-md-12">
				{!! Form::label('formula', 'Формула', ['class' => 'awesome-formlabel']) !!}
	        	{!! Form::text('formula', '', array('class'  => 'awesome-forminput')) !!}
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="col-md-12">
				{!! Form::label('varname', 'Имя переменной', ['class' => 'awesome-formlabel']) !!}
	        	{!! Form::text('varname', '', array('class'  => 'awesome-forminput')) !!}
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="col-md-12">
				{!! Form::label('cost', 'Стоимость (конеч.)', ['class' => 'awesome-formlabel']) !!}
	        	{!! Form::text('cost', '', array('class'  => 'awesome-forminput')) !!}
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-12">
				{!! Form::label('price', 'Цена (закуп.)', ['class' => 'awesome-formlabel']) !!}
	        	{!! Form::text('price', '', array('class'  => 'awesome-forminput')) !!}
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="col-md-12">
				{!! Form::label('factor', 'Множитель', ['class' => 'awesome-formlabel']) !!}
	        	{!! Form::text('factor', '', array('class'  => 'awesome-forminput')) !!}
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-12">
				{!! Form::label('partnum', 'Partnumber', ['class' => 'awesome-formlabel']) !!}
	        	{!! Form::text('partnum', '', array('class'  => 'awesome-forminput')) !!}
	        	</div>
	        </div>

	        	{!! Form::hidden('id') !!}
	        	{!! Form::hidden('level') !!}
	        	{!! Form::hidden('parent_id') !!}
	        	<button class="btn btn-success">Сохранить</button>
	        {!! Form::close() !!}
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>
