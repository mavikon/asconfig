<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/">AS Configurator</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="/ctv" id="ctv-link">Настройки</a></li>
      <li><a href="/dict" id="dict-link">Справочник</a></li>
      {{-- <li><a href="limits" id="limits-link">Ред. ограничений</a></li> --}}
    </ul>
  </div>
</nav>
