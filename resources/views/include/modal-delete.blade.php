	<!-- Modal -->
	<div id="deleteRecordModal" class="modal fade" role="dialog" data-recordToDelete="">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title modal-delete-title"></h4>
	      </div>
	      <div class="modal-body">
	        {!! Form::open(['route'=>'delete.record', 'method' => 'delete', 'id' => 'delRecForm']) !!}

{{--        		@if ($message = Session::get('success-delete'))
				<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>
		    	    <strong>{{ $message }}</strong>
				</div>
			@endif
       		@if ($message = Session::get('error-delete'))
				<div class="alert alert-danger alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>
		    	    <strong>{{ $message }}</strong>
				</div>
			@endif
 --}}
	        <div class="row">
	        	<div class="col-md-12">
					{!! Form::label('name', 'Название', ['class' => 'awesome-formlabel']) !!}
		        	{!! Form::text('name', '', array('class'  => 'awesome-forminput')) !!}
	        	</div>
	        </div>
	        	{!! Form::hidden('id') !!}
	        	<button id="modalDelete-btnSubmit" class="btn btn-danger">Удалить</button>
	        {!! Form::close() !!}
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>
