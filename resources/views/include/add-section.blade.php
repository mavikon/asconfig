<div class="forms-frame add-section">
	{{-- <h4>Добавить раздел</h4> --}}
	{!! Form::open(['route'=>'add.section']) !!}
		@if ($message = Session::get('success-section'))
		<div class="alert alert-success alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>
		        <strong>{{ $message }}</strong>
		</div>
	@endif
	<div class="add-section-form">
		<div class="form-group {{ $errors->has('section_name') ? 'has-error' : '' }}">
			{!! Form::label('Название раздела') !!}
			{!! Form::text('section_name', old('section_name'), ['name'=>'section_name','class'=>'form-control', 'placeholder'=>'Название раздела']) !!}
			<span class="text-danger">{{ $errors->first('section_name') }}</span>
		</div>
		<div class="form-group">
			<button class="btn btn-success">Добавить</button>
		</div>
	</div>
	{!! Form::close() !!}
</div>
