<div class="forms-frame add-category">
	{{-- <h4>Добавить категорию</h4>	 --}}
	{!! Form::open(['route'=>'add.category']) !!}
		@if ($message = Session::get('success-category'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>
			        <strong>{{ $message }}</strong>
			</div>
		@endif
		<div class="add-category-form">

			<div class="select-section form-group {{ $errors->has('parent_id') ? 'has-error' : '' }}">
				{!! Form::label('parent_id', 'Выберите раздел:') !!}

				{!! Form::select(
					'parent_id',
					$mainSections,
					old('categ_id'),
					['class'=>'form-control', 'placeholder'=>'Выберите раздел']
					)
				!!}

				<span class="text-danger">{{ $errors->first('parent_id') }}</span>
			</div>
			<div class="input-half form-group {{ $errors->has('cat_name') ? 'has-error' : '' }}">
				{!! Form::label('cat_name', 'Название категории:') !!}
				{!! Form::text('cat_name', old('cat_name'), ['name'=>'cat_name', 'class'=>'form-control', 'placeholder'=>'Название категории']) !!}
				<span class="text-danger">{{ $errors->first('cat_name') }}</span>
			</div>
			<div class="input-half form-group {{ $errors->has('varname') ? 'has-error' : '' }}">
				<div class="input-quad">
				{!! Form::label('order', '# П/П:') !!}
				{!! Form::text('order', old('order'), ['class'=>'form-control', 'placeholder'=>'#nn']) !!}
				</div>
				<div class="input-quad">
				{!! Form::label('varname', 'Имя переменной:') !!}
				{!! Form::text('varname', old('varname'), ['class'=>'form-control', 'placeholder'=>'Имя переменной']) !!}
				</div>
			</div>
				<div class="input-half form-group {{ $errors->has('cost') ? 'has-error' : '' }}">
				{!! Form::label('cost', 'Стоимость:') !!}
				{!! Form::text('cost', old('cost'), ['class'=>'form-control', 'placeholder'=>'Стоимость']) !!}
			</div>
				<div class="input-half form-group {{ $errors->has('factor') ? 'has-error' : '' }}">
				{!! Form::label('factor', 'Множитель:') !!}
				{!! Form::text('factor', old('factor'), ['class'=>'form-control', 'placeholder'=>'Множитель']) !!}
			</div>
			<div class="form-group">
				<button class="btn btn-success">Добавить</button>
			</div>
		</div>
	{!! Form::close() !!}
</div>
