	<!-- Modal -->
	<div id="infoModal" class="modal fade" role="dialog" data-info="">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title modal-delete-title">Info</h4>
	      </div>
	      <div class="modal-body">
       		@if ($message = Session::get('success-delete'))
				<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>
		    	    <strong>{{ $message }}</strong>
				</div>
			@endif
       		@if ($message = Session::get('error-delete'))
				<div class="alert alert-danger alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>
		    	    <strong>{{ $message }}</strong>
				</div>
			@endif
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>
