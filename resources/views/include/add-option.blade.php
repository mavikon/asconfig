<div class="forms-frame add-option">
	{{-- <h4>Добавить вариант</h4> --}}
	{!! Form::open(['route'=>'add.option']) !!}
		@if ($message = Session::get('success-variant'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>
			        <strong>{{ $message }}</strong>
			</div>
		@endif
		<div class="form-group">
			<label for="sel-cats">Выберите категорию</label>
			<select name="sel-cats" id="sel-cats" class="form-control">
			<option value="" disabled selected>Выберите категорию</option>
				@foreach ($allCategories as $section)
					<optgroup label='{{ $section->name }}'></optgroup>
					@foreach($section->childs as $category)
						<option value='{{ $category->id }}' class='select-option'>{{ $category->name }}</option>
					@endforeach
				@endforeach
			</select>
		</div>
	<div class="add-option-form">
		<div class="input-half form-group {{ $errors->has('variant_name') ? 'has-error' : '' }}">
			{!! Form::label('Название варианта:') !!}
			{!! Form::text('variant_name', old('variant_name'), ['name'=>'variant_name', 'class'=>'form-control', 'placeholder'=>'Название варианта']) !!}
			<span class="text-danger">{{ $errors->first('variant_name') }}</span>
		</div>
		<div class="input-half form-group {{ $errors->has('varname2') ? 'has-error' : '' }}">
			{!! Form::label('Имя переменной:') !!}
			{!! Form::text('varname2', old('varname2'), ['name'=>'varname2', 'class'=>'form-control', 'placeholder'=>'Имя переменной']) !!}
			<span class="text-danger">{{ $errors->first('varname2') }}</span>
		</div>
		<div class="input-half form-group {{ $errors->has('cost2') ? 'has-error' : '' }}">
			{!! Form::label('Стоимость:') !!}
			{!! Form::text('cost2', old('cost2'), ['name'=>'cost2', 'class'=>'form-control', 'placeholder'=>'Стоимость']) !!}
			<span class="text-danger">{{ $errors->first('cost2') }}</span>
		</div>
		<div class="input-half form-group {{ $errors->has('factor2') ? 'has-error' : '' }}">
			{!! Form::label('Множитель:') !!}
			{!! Form::text('factor2', old('factor2'), ['name'=>'factor2', 'class'=>'form-control', 'placeholder'=>'Множитель']) !!}
		</div>

		<div class="input-half form-group {{ $errors->has('price') ? 'has-error' : '' }}">
			{!! Form::label('Цена (закуп.):') !!}
			{!! Form::text('price', old('price'), ['name'=>'price', 'class'=>'form-control', 'placeholder'=>'Цена (закуп.)']) !!}
			<span class="text-danger">{{ $errors->first('price') }}</span>
		</div>
		<div class="input-half form-group {{ $errors->has('partnum') ? 'has-error' : '' }}">
			{!! Form::label('Partnumber:') !!}
			{!! Form::text('partnum', old('partnum'), ['name'=>'partnum', 'class'=>'form-control', 'placeholder'=>'Partnumber']) !!}
		</div>

	</div>
	<div class="form-group add-option-form">
		<button class="btn btn-success">Добавить</button>
	</div>

	{!! Form::close() !!}
</div>
