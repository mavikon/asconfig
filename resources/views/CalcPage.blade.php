<!DOCTYPE html>
<html lang="en">

<head>
    <title>ASConfig main page</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"> --}}
    <link rel="stylesheet" href="/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> --}}
    {{--    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> --}}
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    {{--    <link href="/css/treeview.css" rel="stylesheet"> --}}
    <link rel="stylesheet" href="/css/style.css">
</head>

<body>
    <div class="container">
        <h1>MAIN</h1>
        <h2> Расчет конфигурации {{ $section->name }} </h2>

        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-primary disabled">Печать</button>
                    <button type="button" class="btn btn-primary disabled">E-mail</button>
                    <button id="rstbtn" type="button" class="btn btn-primary ">Сброс</button>

                </div>
            </div>
        </div>

        {!! Form::open() !!}

        <div class="row">
            <div class="col-md-6">

                <div class="panel-group" id="accordion">
                @foreach($categories as $category)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class=""
                                data-toggle="collapse"
                                data-parent="#accordion"
                                href="#collapse{{ $category->id }}">{{ $category->name }}
                                </a>
                            </h4>
                        </div>
                        <div id="collapse{{ $category->id }}" class="panel-collapse collapse">
                            <div class="panel-body">
                                <select class="form-control option-select" id="sel{{ $category->id }}">
                                <option class="selected-bydefault" value="" disabled selected>...</option>
                                    @foreach($category->childs as $option)
                                        <option id="{{ $option->id }}" value="{{ $option->id }}" class="real-option"
                                                data-parentid="{{ $category->id }}"
                                                data-parentname="{{ $category->name }}"
                                                data-section-cost="{{ $section->cost }}"
                                                data-option-name="{{ $option->name }}"
                                                data-category-cost="{{ $category->cost }}"
                                                data-option-cost="{{ $option->cost }}"
                                                data-option-factor="{{ $option->factor }}"
                                                data-limits="{{ $option->limits }}"
                                                data-limitsBy="{{ $option->limits_by }}">
                                            {{ $option->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>

            <div class="col-md-6">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Стоимость</th>
                        </tr>
                    </thead>
                    <tbody id="tr-injection">
                        @foreach($categories as $category)
                            <tr id="{{ $category->id }}"></tr>
                        @endforeach
                    </tbody>
                </table>
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="success">
                            <td>ВЫБРАННАЯ КОНФИГУРАЦИЯ, ИТОГО:</td>
                            <td id="itogo">{{ $section->cost }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        {!! Form::close() !!}

    </div>

{{--         @foreach($categories as $category)
            <h3>{{ $category->name }}</h3>
        @endforeach
 --}}
        {{-- {{ $categories }} --}}
        <script src="/js/main.js"></script>
</body>

</html>