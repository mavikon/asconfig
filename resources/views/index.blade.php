<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>AS Configurator</title>

    <link rel="stylesheet" href="/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> --}}

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    {{--    <link href="/css/treeview.css" rel="stylesheet"> --}}
    <link rel="stylesheet" href="/css/style.css">

</head>
<body>

	@include('include.navbar')
	{{-- <div id="app"><example></example></div> --}}
	<div class="container">
		<h1>Конфигуратор</h1>
		<h2>Как пользоваться</h2>
		<h5><a href="https://bitbucket.org/mavikon/asconfig">Git repository on BitBucket</a></h5>
		<h4>(это прототип, оформление и верстка не выполнялись)</h4>
		<p>Сначала нужно создать <code>раздел</code>, название которого, в дальнейшем, будет являться частью URL страницы с интерфейсом пользователя. Это делается в <a href="/ctv"><code>настройках</code></a><br>
		Например, мы хотим создать страничку для подсчета конфигурации ДСХД. Тогда, в настройках, создаем новый раздел с названием <code>ДСХД</code>
		Теперь, обратившись по адресу <a href="/дсхд"><code>/дсхд</code></a> мы попадем на страничку подсчета конфигурации <code>ДСХД</code> <br>
		Чтобы на такой страничке появились <code>категории</code> и <code>варианты</code> из которых можно "собирать" конфигурацию, нужно их также добавить через интерфейс <a href="/ctv"><code>настроек</code></a>. <br>
		В конфигурацию попадают только <code>варианты</code>, т.е. самый нижний уровень в иерархии. <br></p>
		<p>Существует возможность описать правила, ограничивающие выбор каких-либо вариантов вместе с другими. После создания таких правил, при выборе варианта, ограничивающего выбор, запрещенные варианты становятся недоступны. Описать правила можно на странице <a href="/limits"><code>Ред.ограничений</code></a></p>
		<p>Цена на страничке подсчета конфигурации вычисляется (пока) по формуле <code>(СТ_КАТ+СТ_ВАР)*МНОЖИТЕЛЬ</code>, где
		<ul class="list-group">
			<li class="list-group-item"><b>СТ_КАТ</b> - стоимость, заданная для всей категории (по умолчанию = 0) </li>
			<li class="list-group-item"><b>СТ_ВАР</b> - стоимость, задаваемая при создании варианта</li>
			<li class="list-group-item"><b>МНОЖИТЕЛЬ</b> - коэффициент, задаваемый при создании варианта (по умолчанию = 1)</li>
		</ul>

		</p>

		<h2>Ближайшие планы:</h2>
		<ul class="list-group">
			<li class="list-group-item task-done">Возможность редактирования и удаления сущностей в настройках</li>
			<li class="list-group-item task-done">Возможность описания ограничений, когда какой-либо вариант не может быть выбран вместе с другим</li>
			<li class="list-group-item">Ввести поле, задающее порядок сортировки вывода <code>Категорий</code></li>
			<li class="list-group-item">Возможность задать формулу расчета конфигурации</li>
			<li class="list-group-item">Использование данных из справочника в расчетах (курса валют например)</li>
			<li class="list-group-item task-done">Возможность редактирования и удаления записей в справочнике</li>
			<li class="list-group-item">Учитывать скидку при подсчете</li>
		</ul>
		<h2>Что нужно еще:</h2>
		<ul class="list-group">
			<li class="list-group-item">Возможность указывать количество при выборе варианта (если это нужно вообще)</li>
			<li class="list-group-item">Сохранять результат</li>
			<li class="list-group-item">отправлять на емайл</li>
			<li class="list-group-item">создавать запрос администратору сайта (форма обратной связи?)</li>
		</ul>
	</div>
{{-- <script src="{{ asset('js/app.js') }}"></script> --}}
</body>
</html>