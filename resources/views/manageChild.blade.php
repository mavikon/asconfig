<ul>
@foreach($childs as $child)
	@if($child->type = 'OPTION')
	<li class="variant">
	@else
	<li>
	@endif
        @if(count($child->childs))
            <i class="fa fa-plus-circle" aria-hidden="true"></i>
        @endif
	    {{ $child->name }}
        <code id="{{ $child->id }}"
              data-id="{{ $child->id }}"
        	  data-recordName="{{ $child->name }}"
        	  data-recordParentId="{{ $child->parent_id }}"
              data-hasChilds="{{ count($child->childs) }}"
        	  type="button"
        	  class="btn-delete btn-xs btn-default">Delete</code>
        <code id="{{ $child->id }}"
              data-id="{{ $child->id }}"
        	  data-recordName="{{ $child->name }}"
              data-recordOrder="{{ $child->order }}"
        	  data-recordType="{{ $child->type }}"
        	  data-recordLevel="{{ $child->level }}"
        	  data-recordFormula="{{ $child->formula }}"
        	  data-recordVarname="{{ $child->varname }}"
        	  data-recordCost="{{ $child->cost }}"
        	  data-recordFactor="{{ $child->factor }}"
        	  data-recordParentId="{{ $child->parent_id }}"
              data-recordPrice="{{ $child->price }}"
              data-recordPartnum="{{ $child->partnum }}"
        	  type="button"
        	  class="btn-edit btn-xs btn-default">Edit</code>

	    @if(count($child->childs))
            @include('manageChild',['childs' => $child->childs])
        @endif
	</li>
@endforeach
</ul>