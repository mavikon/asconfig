<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>ASConfig Limitations page</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> --}}
    {{-- <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> --}}
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="/css/treeview.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://npmcdn.com/vue/dist/vue.js"></script>

</head>
<body>
	@include('include.navbar')
	<div class="container">
		<div class="panel panel-primary main-panel">
			<div class="panel-heading">Панель администратора ver. 0.0.1a</div>
				<div id="vuejs-app" class="panel-body">
					<div class="row">
						<div class="col-md-12">

							<h1>
								Описание ограничений для раздела {{ $par }}
{{-- 								<select class="my-form-control" v-model="section" >
									@if ($par == '')
										<option value="" disabled selected>&nbsp;&nbsp;...</option>
									@endif
									@foreach ($allCategories as $section)
										@if ($section->name == $par)
											<option value="{{ $section->name }}" selected>
												&nbsp;{{ $section->name }}&nbsp;
											</optgroup>
										@else
											<option value="{{ $section->name }}">
												&nbsp;{{ $section->name }}&nbsp;
											</optgroup>
										@endif
									@endforeach
								</select>
 --}}							</h1>
							<h4><code>&lt;Вариант 1&gt;</code> нельзя будет использовать, если выбран <code>&lt;Вариант 2&gt;</code></h4>
							{!! Form::open(['route'=>'add.limit']) !!}
							<div id="limits-div">
							<table id="limits-table" class="table table-striped table-hover table-responsive">
								<thead>
									<tr>
										<th>Вариант 1</th>
										<th>Вариант 2</th>
									</tr>
								</thead>
								<tbody>
									@foreach($limits as $limit)
									<tr id="lrid{{ $limit->id }}"
										:class="{hidden: activeSection('{{ $limit->subj_name_pp }}', '{{ $par }}')}">
										<td>{{ $limit->subj_name_pp }}
											<i class="fa fa-arrow-right" aria-hidden="true"></i>
											{{ $limit->subj_name_p }}
											<i class="fa fa-arrow-right" aria-hidden="true"></i>
											{{ $limit->subj_name }}
										</td>
										<td>{{ $limit->limit_name_pp }}
											<i class="fa fa-arrow-right" aria-hidden="true"></i>
											{{ $limit->limit_name_p }}
											<i class="fa fa-arrow-right" aria-hidden="true"></i>
											{{ $limit->limit_name }}
										</td>
										<td>
											<button id="btn-limits-delete{{ $limit->id }}"
												type="button"
												class="btn btn-danger btn-xs btn-limits-delete"
												data-recordId="{{ $limit->id }}">
												<i class="fa fa-trash-o" aria-hidden="true"></i>
											</button>
										</td>
									</tr>
									@endforeach
								</tbody>
								<tfoot>
									<tr>
										<th>
											<select name="sel-subjId" id="sel-subjId" class="form-control"
											v-on:change="optChanged($event, 0)">
											<option value="" disabled selected>Выберите вариант</option>
												@foreach ($allCategories as $section)
													{{-- <optgroup label='{{ $section->name }}'></optgroup> --}}
													@foreach($section->childs as $category)
														<option
														:class="{hidden: activeSection('{{ $section->name }}', '{{ $par }}')}"
														value="{{ $category->id }} # {{ $section->name }} -> {{ $category->name }}" class='select-subjName'>{{ $section->name }} -> {{ $category->name }}</option>
													@endforeach
												@endforeach
											</select>
											<span class="text-danger">{{ $errors->first('sel-subjId') }}</span>
										</th>
										<th>
											<select name="sel-limitId" id="sel-limitId" class="form-control"
											v-on:change="optChanged($event, 1)" >
												<option value="" disabled selected>Выберите вариант</option>
													@foreach ($allCategories as $section)
														{{-- <optgroup label='{{ $section->name }}'></optgroup> --}}
														@foreach($section->childs as $category)
															<option
															:class="{hidden: activeSection('{{ $section->name }}','{{ $par }}')}"
															value="{{ $category->id }} # {{ $section->name }} -> {{ $category->name }}" class='select-limitName'>{{ $section->name }} -> {{ $category->name }}</option>
														@endforeach
													@endforeach
											</select>
											<span class="text-danger">{{ $errors->first('sel-limitId') }}</span>
										</th>
										<th>
											<button
												id="btn-limits-save" type="button"
												class="btn btn-success btn-xs"
												v-on:click="btnLimitsSave">
												<i class="fa fa-floppy-o" aria-hidden="true"></i>
											</button>
										</th>
									</tr>
								</tfoot>
							</table>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="checkboxes-option1">
										<div class="container-fluid">
											<div class="panel panel-default">
												<div class="panel-body">
													<div v-for="n in response1" :id="'div' + n.id" class="checkbox">
														<label><input type="checkbox"
															:id="'input' + n.id"
															:value="n.id"
															v-model="checkedCategory1">@{{ n.name }}</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								{{-- <div class="col-md-1"></div> --}}
								<div class="col-md-6">
									<div class="checkboxes-option2">
										<div class="container-fluid">
											<div class="panel panel-default">
												<div class="panel-body">
													<div v-for="n in response2" :id="'div' + n.id" class="checkbox">
														<label><input type="checkbox"
															:id="'input' + n.id"
															:value="n.id"
															v-model="checkedCategory2">@{{ n.name }}</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <script src="/js/manageLimits.js"></script>
    <script src="/js/treeview.js"></script>
    <script src="/js/main.js"></script>

</body>
</html>