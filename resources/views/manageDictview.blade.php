<!DOCTYPE html>
<html>
<head>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>ASConfig admin page</title>
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> --}}
    {{-- <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> --}}
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="/css/treeview.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>

	@include('include.navbar')
	@include('include.modal-edit-dict')


	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading">Панель администратора ver. 0.0.1a</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<h4>Справочник (key->value)</h4>
						{!! Form::open(['route'=>'add.dict']) !!}
						<table id="table-dict" class="table table-striped">
						<thead>
							<tr><th>#</th><th>КЛЮЧ</th><th>ЗНАЧЕНИЕ</th><th>КОММЕНТАРИЙ</th><th>Created</th><th>Updated</th></tr>
						</thead>
						<tbody>

						@foreach($dicts as $dict)
							<tr id="rid{{ $dict->id }}">
								<td>{{ $dict->id }}</td>
								<td>{{ $dict->key }}</td>
								<td>{{ $dict->value }}</td>
								<td>{{ $dict->comment }}</td>
								<td>{{ $dict->created_at }}</td>
								<td>{{ $dict->updated_at }}</td>
								<td>
									<button type="button" class="btn btn-info btn-xs btn-edit-dict"
										data-recordKey="{{ $dict->key }}"
										data-recordValue="{{ $dict->value }}"
										data-recordComment="{{ $dict->comment }}"
										data-recordId="{{ $dict->id }}">
										<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
									</button>
								</td>
								<td>
									<button type="button" class="btn btn-danger btn-xs btn-delete-dict" data-recordId="{{ $dict->id }}">
										<i class="fa fa-trash-o" aria-hidden="true"></i>
									</button>
								</td>
							</tr>
						@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>#</th>
								<th>
									{{-- {!! Form::label('Название раздела') !!} --}}
									{!! Form::text('key', old('key'), ['class'=>'form-control', 'placeholder'=>'КЛЮЧ']) !!}
									<span class="text-danger">{{ $errors->first('key') }}</span>
								</th>
								<th>
									{!! Form::text('value', old('value'), ['class'=>'form-control', 'placeholder'=>'ЗНАЧЕНИЕ']) !!}
									<span class="text-danger">{{ $errors->first('value') }}</span>
								</th>
								<th>{!! Form::text('comment', old('comment'), ['class'=>'form-control', 'placeholder'=>'КОММЕНТАРИЙ']) !!}</th>
								<th>Created</th>
								<th>Updated</th>
							</tr>
						</tfoot>
						</table>
						<button type="submit" class="btn btn-success">Добавить</button>
						<a href="/ctv" class="btn btn-success" role="button">Назад</a>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
	  		</div>
        </div>
    </div>
    <script src="/js/treeview.js"></script>
    <script src="/js/main.js"></script>
</body>
</html>