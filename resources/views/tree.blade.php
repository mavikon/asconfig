<!DOCTYPE html>
<html>
<head>
	<title>ASConfig main page</title>
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> --}}
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="/css/treeview.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
	<div class="container">
		<div class="panel panel-primary main-panel">
			<div class="panel-heading">AS Configurator ver. 0.0.1a</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
	  					<h3>Раздел-&gt;Категория-&gt;Вариант</h3>
				        <ul id="tree1">
				            @foreach($categories as $category)
				            	<li>
                                    @if(count($category->childs))
                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                    @endif
				                    {{ $category->name }}
				                    @if(count($category->childs))
				                        @include('manageChild',['childs' => $category->childs])
				                    @endif
				                </li>
				            @endforeach
				        </ul>
	  				</div>
					<div class="col-md-6">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Вариант</th>
									<th>Стоимость</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>[ДСХД] Тип корпуса - 2U</td>
									<td>24,500.00 руб.</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="/js/treeview.js"></script>
</body>
</html>