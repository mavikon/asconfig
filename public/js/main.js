/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 45);
/******/ })
/************************************************************************/
/******/ ({

/***/ 45:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(46);


/***/ }),

/***/ 46:
/***/ (function(module, exports) {

$(document).ready(function () {

    function s(par) {
        alert(par);
    }
    // $("p").click(function(){
    //     $(this).hide();
    // });
    function calculateSum(startPrice) {
        var sum = 0;

        sum = +sum + +startPrice;
        // iterate through each td based on class and add the values
        $(".price").each(function () {
            var value = $(this).text();
            // add only if the value is number
            if (!isNaN(value) && value.length != 0) {
                sum += parseFloat(value);
            }
        });
        $("#itogo").html(sum);
    }

    // $(calculateSum(
    //         $( this ).find(":selected").attr("data-section-cost")
    //     )
    // );

    $("#rstbtn").click(function (event) {
        /* Act on the event */
        location.reload();
        $(".selected-bydefault").prop('selected', true);
    });

    $(".option-select").change(function () {
        var categoryCost = $(this).find(":selected").attr("data-category-cost");
        var categoryName = $(this).find(":selected").attr("data-parentname");
        var optionCost = $(this).find(":selected").attr("data-option-cost");
        var optionFactor = $(this).find(":selected").attr("data-option-factor");
        var parentId = $(this).find(":selected").attr("data-parentid");
        var cost = (parseFloat(categoryCost) + parseFloat(optionCost)) * parseFloat(optionFactor);
        var prevId = $('tr[id=' + $(this).find(":selected").attr("data-parentid") + ']').prev().attr('id');

        var limits = JSON.parse($(this).find(":selected").attr("data-limits"));
        var limitsBy = JSON.parse($(this).find(":selected").attr("data-limitsBy"));

        // check if ($('#'+prevId + ' td').length == 0) && ($('#'+prevId).length != 0) 
        // then previous position not selected
        // and selection is not allowed
        if ($('#' + prevId + ' td').length == 0 && $('#' + prevId).length != 0) {
            alert('Нельзя выбрать [' + categoryName + '] без выбора предыдущей позиции!');
            $('#sel' + parentId + ' .selected-bydefault').prop('selected', true);
            return;
        }

        /**
        * check for limits
        */

        var arrLimits = [];
        for (var i = 0, len = limits.length; i < len; i++) {
            // here set disabled props to option with id = limits[i].subj_id
            // $('#'+limits[i].subj_id).prop('disabled', true);
            arrLimits.push(limits[i].subj_id);
            // console.log('#'+limits[i].subj_id);
        }
        var arrLimitsBy = [];
        for (var i = 0, len = limitsBy.length; i < len; i++) {
            arrLimitsBy.push(limitsBy[i].limit_id);
            // console.log('limit_id: ' + limitsBy[i].limit_id);
        }

        // console.log('limits: ' +limits + '(length: ' + arrLimits.length + ')');
        // console.log('limitsBy: ' +limitsBy + '(length: ' + arrLimitsBy.length + ')');

        $('.real-option').each(function (index, el) {
            // пробегаемся по всем вариантам и делаем что-то полезное
            // 1. remove 'disabled' (it's not right but just for now)
            // $('#'+$(this).val()).prop('disabled', false);
            // 2. set 'disabled' to true if $(this).val() IN arrLimits
            // console.log(+$(this).val() + ' ' + arrLimits[0] + ' ' + arrLimits[1] + ' ' + arrLimits[2] + ' ' + arrLimits[3] + ' ' + $.inArray($(this).val(), arrLimits) );
            if ($.inArray(+$(this).val(), arrLimits) > -1) {
                $('#' + $(this).val()).prop('disabled', true);
            }
            // console.log($(this).val());
        });

        /** check if <tr> with "data-parentid" already exists
        * if YES, update <tr> 
        * else append with new <tr>
        */
        if ($("#" + $(this).find(":selected").attr("data-parentid")).length) {
            $("#" + $(this).find(":selected").attr("data-parentid")).html("\n                            <td id=\"" + $(this).val() + "\">" + $(this).val() + "</td>\n                            <td>" + $(this).find(":selected").attr("data-parentname") + " => " + $(this).find(":selected").text() + "</td>\n                            <td class=\"price\">" + cost + "</td>\n                    ");
        } else {
            $("#tr-injection").append("\n                <tr id=\"" + $(this).find(":selected").attr("data-parentid") + "\">\n                    <td id=\"" + $(this).val() + "\">" + $(this).val() + "</td>\n                    <td>" + $(this).find(":selected").attr("data-parentname") + " => " + $(this).find(":selected").text() + "</td>\n                    <td class=\"price\">" + cost + "</td>\n                </tr>\n            ");
        }
        /**
        * "itogo" calculation
        */
        $(calculateSum($(this).find(":selected").attr("data-section-cost")));

        $("a[href='#collapse" + parentId + "']").addClass('not-active');
        $("a[href='#collapse" + parentId + "']").collapse();
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".btn-edit-dict").click(function (e) {
        e.preventDefault();
        /* Act on the event */
        $("#editDictModal input[name='id']").val($(this).attr('data-recordId'));
        $("#editDictModal input[name='key']").val($(this).attr('data-recordKey'));
        $("#editDictModal input[name='value']").val($(this).attr('data-recordValue'));
        $("#editDictModal input[name='comment']").val($(this).attr('data-recordComment'));
        $('#editDictModal').modal('show');
    });

    $(".btn-delete-dict").click(function (e) {
        e.preventDefault();
        /* Act on the event */
        $.ajax({
            url: '/del-dict',
            type: 'POST',
            dataType: 'json',
            data: {
                _method: 'DELETE',
                id: $(this).attr('data-recordId') },
            success: function success(msg) {
                $("#rid" + msg['id']).remove();
                console.log("rid" + msg['id']);
            }
        }).done(function () {
            //
        }).fail(function () {
            console.log("error .btn-delete-dict click event");
        }).always(function () {
            //console.log("complete");
        });
    });

    $(".btn-limits-delete").click(function (e) {
        e.preventDefault();
        /* Act on the event */
        $.ajax({
            url: '/del-limits',
            type: 'POST',
            dataType: 'json',
            data: {
                _method: 'DELETE',
                id: $(this).attr('data-recordId') },
            success: function success(msg) {
                $("#lrid" + msg['id']).remove();
                console.log("lrid" + msg['id']);
            }
        }).done(function () {
            //
        }).fail(function () {
            console.log("error .btn-limits-delete click event");
        }).always(function () {
            //console.log("complete");
        });
    });

    $("#ctv-link").click(function (event) {
        event.preventDefault();
        /* Act on the event */
        location.href = "/ctv";
    });
    $("#dict-link").click(function (event) {
        event.preventDefault();
        /* Act on the event */
        location.href = "/dict";
    });
    // $("#limits-link").click(function(event) {
    //     event.preventDefault();
    //     /* Act on the event */
    //     location.href="limits";
    // });
});

/***/ })

/******/ });