/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 47);
/******/ })
/************************************************************************/
/******/ ({

/***/ 47:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(48);


/***/ }),

/***/ 48:
/***/ (function(module, exports) {

// require('./bootstrap');

var apiURL = '/getByParentId/';
var apiURL_POST = '/add-limit';
var token = document.head.querySelector('meta[name="csrf-token"]');

if (!token) {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

new Vue({
    el: '#vuejs-app',
    data: {
        value: [],
        checkedCategory1: [],
        checkedCategory2: [],
        response1: null,
        response2: null,
        categorySelectedId1: null,
        categorySelectedId2: null,
        section: ''
    },
    methods: {
        activeSection: function activeSection(sectionName, par) {
            // console.log(par);
            if (sectionName == par) return false;

            return true;
        },
        btnLimitsSave: function btnLimitsSave() {
            // 1. Check if left panel NOT the same as right panel
            if (typeof this.value[0] == 'undefined' || typeof this.value[1] == 'undefined' || this.checkedCategory1.length == 0 || this.checkedCategory2.length == 0) {
                alert('Необходимо выбрать категории и отметить варианты');
                return false;
            }
            if (+this.value[0].split(' ', 1) == +this.value[1].split(' ', 1)) {
                alert('Нельзя выбирать одинаковые категории');
                return false;
            }
            //
            // 2. if one or more checkboxes are checked at the left panel and at the right panel
            // THEN do work, else exception handling
            for (var i = 0, len2 = this.checkedCategory2.length; i < len2; i++) {
                for (var j = 0, len1 = this.checkedCategory1.length; j < len1; j++) {
                    this.pushData(this.checkedCategory1[j], this.checkedCategory2[i]);
                }
            }
        },
        pushData: function pushData(option1, option2) {
            var sendPar = JSON.stringify({ "sel-subjId": option1, "sel-limitId": option2 });
            var xhr = new XMLHttpRequest();
            var self = this;
            xhr.open('POST', apiURL_POST);
            xhr.setRequestHeader("Content-type", "application/json");
            xhr.setRequestHeader('X-CSRF-TOKEN', token.content);
            xhr.onreadystatechange = function () {
                //Call a function when the state changes.
                if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
                    // Request finished. Do processing here.
                    console.log('Request finished. Data send: ' + sendPar);
                    location.reload();
                }
            };
            xhr.send(sendPar);
        },
        fetchData: function fetchData(par, index) {
            var xhr = new XMLHttpRequest();
            var self = this;
            xhr.open('GET', apiURL + par);
            xhr.onload = function () {
                if (index == 0) self.response1 = JSON.parse(xhr.responseText);else if (index == 1) self.response2 = JSON.parse(xhr.responseText);
            };
            xhr.send();
        },
        optChanged: function optChanged(event, index) {
            this.value[index] = event.target.value;
            this.fetchData(this.value[index].split(' ', 1), index);
        }
    }
});

/***/ })

/******/ });