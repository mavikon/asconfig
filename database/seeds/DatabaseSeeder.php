<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Eloquent::unguard();

		// $this->call(UsersTableSeeder::class);
		$this->call('AsConfigSeeder');
		$this->command->info('AsConfig seeds finished.');
	}
}

class AsConfigSeeder extends Seeder {
	public function run() {
		// clear database
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		DB::table('categories')->delete();
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');

		$cat1 = App\Category::create(array('name' => 'ДСХД', 'type' => 'SECTION', 'level' => 1, 'formula' => '', 'parent_id' => null));
		$cat2 = App\Category::create(array('name' => 'Тип корпуса', 'type' => 'CATEGORY', 'level' => $cat1->level + 1, 'parent_id' => $cat1->id, 'varname' => "CASETYPE"));
		$cat3 = App\Category::create(array('name' => '1U', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'cost' => 7340.50));
		$cat3 = App\Category::create(array('name' => '2U', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'cost' => 22340.50));
		$cat3 = App\Category::create(array('name' => '4U', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'cost' => 42340.50));
		$cat2 = App\Category::create(array('name' => 'Количество HDD', 'type' => 'CATEGORY', 'level' => $cat1->level + 1, 'parent_id' => $cat1->id, 'cost' => 7654.50));
		$cat3 = App\Category::create(array('name' => '4x HDD', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'varname' => "{'HDD'} * {'factor'}", 'factor' => 4));
		$cat3 = App\Category::create(array('name' => '8x HDD', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'varname' => "{'HDD'} * {'factor'}", 'factor' => 8));
		$cat3 = App\Category::create(array('name' => '12x HDD', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'varname' => "{'HDD'} * {'factor'}", 'factor' => 12));
		$cat3 = App\Category::create(array('name' => '24x HDD', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'varname' => "{'HDD'} * {'factor'}", 'factor' => 24));
		$cat3 = App\Category::create(array('name' => '36x HDD', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'varname' => "{'HDD'} * {'factor'}", 'factor' => 36));
		$cat2 = App\Category::create(array('name' => 'CPU', 'type' => 'CATEGORY', 'level' => $cat1->level + 1, 'parent_id' => $cat1->id));
		$cat3 = App\Category::create(array('name' => '1x Xeon E3', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'varname' => "{'CPU_E3'} * {'factor'}", 'cost' => 7654.50));
		$cat3 = App\Category::create(array('name' => '2x Xeon E3', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'varname' => "{'CPU_E3'} * {'factor'}", 'cost' => 14654.50));
		$cat3 = App\Category::create(array('name' => '1x Xeon E5', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'varname' => "{'CPU_E5'} * {'factor'}", 'cost' => 17654.50));
		$cat3 = App\Category::create(array('name' => '2x Xeon E5', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'varname' => "{'CPU_E5'} * {'factor'}", 'cost' => 34654.50));
		$cat2 = App\Category::create(array('name' => 'RAM', 'type' => 'CATEGORY', 'level' => $cat1->level + 1, 'parent_id' => $cat1->id, 'varname' => 'RAM'));
		$cat3 = App\Category::create(array('name' => '8 Gb', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'cost' => 2654.50));
		$cat3 = App\Category::create(array('name' => '16 Gb', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'cost' => 4654.50));
		$cat3 = App\Category::create(array('name' => '32 Gb', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'cost' => 7654.50));
		$cat3 = App\Category::create(array('name' => '64 Gb', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'cost' => 17654.50));
		$cat2 = App\Category::create(array('name' => 'Cache', 'type' => 'CATEGORY', 'level' => $cat1->level + 1, 'parent_id' => $cat1->id, 'varname' => 'CACHE'));
		$cat3 = App\Category::create(array('name' => 'Нет', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id));
		$cat3 = App\Category::create(array('name' => '32 Gb', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'cost' => 7654.50));
		$cat3 = App\Category::create(array('name' => '64 Gb', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'cost' => 17654.50));
		$cat3 = App\Category::create(array('name' => '128 Gb', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'cost' => 27654.50));

		$cat2 = App\Category::create(array('name' => 'Операционная система', 'type' => 'CATEGORY', 'level' => $cat1->level + 1, 'parent_id' => $cat1->id, 'varname' => 'OS'));

		$cat3 = App\Category::create(array('name' => 'Нет', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id));
		$cat3 = App\Category::create(array('name' => 'AS-Engine', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'cost' => 7654.50));
		$cat3 = App\Category::create(array('name' => 'Linux', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id));
		$cat2 = App\Category::create(array('name' => 'Купон на скидку', 'type' => 'CATEGORY', 'level' => $cat1->level + 1, 'parent_id' => $cat1->id, 'varname' => 'DISCONT'));
		$cat3 = App\Category::create(array('name' => 'Нет', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'factor' => 1.0));
		$cat3 = App\Category::create(array('name' => '10%', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'factor' => 0.1));
		$cat3 = App\Category::create(array('name' => '15%', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'factor' => 0.15));
		$cat1 = App\Category::create(array('name' => 'LP', 'type' => 'SECTION', 'level' => '1', 'parent_id' => null));
		$cat2 = App\Category::create(array('name' => 'Купон на скидку', 'type' => 'CATEGORY', 'level' => $cat1->level + 1, 'parent_id' => $cat1->id));
		$cat3 = App\Category::create(array('name' => 'Нет', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'factor' => 1.0));
		$cat3 = App\Category::create(array('name' => '10%', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'factor' => 0.1));
		$cat3 = App\Category::create(array('name' => '15%', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'factor' => 0.15));
		$cat2 = App\Category::create(array('name' => 'использование CMS', 'type' => 'CATEGORY', 'level' => $cat1->level + 1, 'parent_id' => $cat1->id));
		$cat3 = App\Category::create(array('name' => 'Нет', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id));
		$cat3 = App\Category::create(array('name' => 'Wordpress', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id));
		$cat3 = App\Category::create(array('name' => 'Joomla', 'type' => 'CATEGORY', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id));

		$cat2 = App\Category::create(array('name' => 'интернет магазин', 'type' => 'CATEGORY', 'level' => $cat1->level + 1, 'parent_id' => $cat1->id));

		$cat3 = App\Category::create(array('name' => 'Нет', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id));
		$cat3 = App\Category::create(array('name' => 'Есть', 'type' => 'OPTION', 'level' => $cat2->level + 1, 'parent_id' => $cat2->id, 'cost' => 2340.50));

		$cat2 = App\Category::create(array('name' => 'Хостинг', 'type' => 'CATEGORY', 'level' => $cat1->level + 1, 'parent_id' => $cat1->id));

		$cat1 = App\Category::create(array('name' => 'CallShark', 'type' => 'SECTION', 'level' => '1', 'parent_id' => null));
	}
}