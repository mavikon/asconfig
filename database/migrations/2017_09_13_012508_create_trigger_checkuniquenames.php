<?php

use Illuminate\Database\Migrations\Migration;

class CreateTriggerCheckuniquenames extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		DB::unprepared("
            create trigger `check_non_unique_names_trigger` before insert on `categories`
            for each row
            begin
                IF new.`name` IN (
                    select c.`name`
                    from `categories` c
                    where (NEW.`name` = c.`name` AND IFNULL(NEW.`parent_id`,0) = IFNULL(c.`parent_id`,0) AND NEW.`level` = c.`level`)
                ) THEN
                SIGNAL SQLSTATE '45000'
                    SET MESSAGE_TEXT = 'ОШИБКА - Попытка добавить запись с именем, которое уже существует в том же разделе или категории!';
                end if;
            end;");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		DB::unprepared('DROP TRIGGER `check_non_unique_names_trigger`');
	}
}
