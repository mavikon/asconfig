<?php

use Illuminate\Database\Migrations\Migration;

class CreateTrigger extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		DB::unprepared('
        create trigger `auto_order_categories_trigger` before insert on `categories`
        for each row
        begin
            DECLARE categ_count INT;
            IF new.`level` = 2 THEN
            SET categ_count = (select count(id) from `categories` where level=2 and parent_id=NEW.parent_id );
            set NEW.`order` = categ_count + 1;
            end if;
        end');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		DB::unprepared('DROP TRIGGER `auto_order_categories_trigger`');
	}
}
