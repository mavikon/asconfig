let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.js('resources/assets/js/treeview.js', 'public/js')
	.js('resources/assets/js/main.js', 'public/js')
	.js('resources/assets/js/manageLimits.js', 'public/js')
   .sass('resources/assets/sass/style.sass', 'public/css')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/sass/404.sass', 'public/css')
   .browserSync('localhost:8000');

